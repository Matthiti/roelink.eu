# roelink.eu

This is the git repository of my **old** website.

## Start the development server

```shell
npm run dev
```

## Build

```shell
npm run build
npm run export
```
