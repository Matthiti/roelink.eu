# CHANGELOG

## [3.7.4] - 2021-09-28

### Added

- Link to blog

## [3.7.3] - 2021-08-06

### Changed

- Fix redirect issue

- Remove trailing slash in some routes

## [3.7.2] - 2021-08-04

### Changed

- Introduction text

## [3.7.1] - 2021-08-01

### Changed

- Show all projects on homepage

- Use server-side rendering for API calls

### Removed

- Projects page

## [3.7.0] - 2021-07-31

### Added

- Project pages

## [3.6.1] - 2021-05-07

### Fixed

- Fixed printing style of resume

## [3.6.0] - 2021-05-06

### Added

- ScrollActive

### Changed

- Primary color to green

- Remove automatic dark mode detection, now always show in dark mode

- Removed underline of certain links

- Spacing in resume page

### Fixed

- Dark mode in navbar

## [3.5.0] - 2021-04-21

### Added

- Interests section

### Removed

- Skills section

## [3.4.4] - 2021-02-19

### Added

- Bind'r veur oe website to portfolio

## [3.4.3] - 2021-01-28

### Changed

- Clicking on my name in the header now takes you to the home page

## [3.4.2] - 2021-01-27

### Changed

- Fixed navbar hidden in light mode

## [3.4.1] - 2021-01-23

### Changed

- Return proper 404 status code on not found page

## [3.4.0] - 2021-01-17

### Added

- Custom error page

### Changed

- `a` elements referencing to other page of my website replaced with `NuxtLink`

- Removed `default` layout, all non-error pages should now use the `page` layout

- Don't open research page in a new tab

## [3.3.1] - 2021-01-04

### Changed

- Moved research to portfolio

## [3.3.0] - 2021-01-02

### Changed

- More mobile friendly home page

- Switched order of `Skills` and `Projects`

- Added and removed skills and projects

## [3.2.1] - 2020-11-06

### Changed

- Changed `a` tags with `mailto` to hide from email crawlers

## [3.2.0] - 2020-10-22

### Added

- Skills section to resume

## [3.1.1] - 2020-08-28

### Added

- Google Site Verification tag to head for indexing

## [3.1.0] - 2020-08-17

### Removed

- TypeScript support

## [3.0.4] - 2020-08-17

### Changed

- Updated student information

## [3.0.3] - 2020-08-13

### Added

- Loading state when the resume is loading

## [3.0.2] - 2020-07-12

### Fixed

- Hamburger menu is working again, but without animation

## [3.0.1] - 2020-07-08

### Added

- Roelink.eu to "My Projects" section

## [3.0.0] - 2020-07-04

### Changed

- Rebuild the whole website, now using NuxtJS

## [2.5.0] - 2020-07-03

### Added

- Dark mode support

## [2.4.1] - 2020-07-02

### Added

- Video to the research page

## [2.4.0] - 2020-06-27

### Added

- Research page

### Removed

- `vendor` folder from Git

## [2.3.0] - 2020-04-16

### Added

- Portfolio section

## [2.2.0] - 2020-03-27

### Changed

- Introduction about myself

## [2.1.2] - 2020-03-27

### Added

- Gitlab link in resume page

### Removed

- Facebook links

## [2.1.1] - 2020-02-19

### Changed

- Set year of copyright automatically using JavaScript

## [2.1.0] - 2019-12-15

### Changed

- cv.html and cv.js to correspond to the new backend
- font family is now Sans Serif

## [2.0.2] - 2019-12-09

### Added

- Link to "Code of Advent 2019"-project
- Gitlab icon to contact

## [2.0.1] - 2019-12-08

### Added

- Print button for resume

## [2.0.0] - 2019-12-07

### Added

- My name on the navigation bar
- Github to footer
- "Herman" to projects

### Changed

- Rebuild the basis again, now with proper npm, gulp and SCSS usage.

### Removed

- C# skill

## [1.0.4] - 2019-05-07

### Added

- Resume

## [1.0.3] - 2019-05-05

### Added

- `Vue.js` to Skills

## [1.0.2] - 2019-02-01

### Added

- `The Great Raspberry Pi Guide` to My Projects

## [1.0.1] - 2019-01-13

### Added
- `Raspberry Pi` to Skills
- `Linux` to Skills
- `SQL` to Skills

### Changed
- Upgraded Font Awesome and use web version instead of local version.
- Removed unused links (scripts, stylesheets, etc.).
- Removed unused files in `vendor`.

## [1.0.0] - 2019-01-04

### Added
- Initial release
- gitlab-ci.yml file
